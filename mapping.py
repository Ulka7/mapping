import folium
import pandas


features_volcanoes = folium.FeatureGroup(name='Volcanoes')

volcanoes = pandas.read_csv('Volcanoes.txt')
latitude = list(volcanoes["LAT"])
longitude = list(volcanoes["LON"])
elevation = list(volcanoes['ELEV'])
name = list(volcanoes["NAME"])

def colorize(elevation):
    if elevation < 1000:
        return 'green'
    elif 1000 <= elevation < 3000:
        return 'orange'
    else:
        return 'red'

map = folium.Map(location=[38, -99], zoom_start=4, tiles= 'Stamen Terrain')


html = """
Volcano name:<br>
<a href="https://www.google.com/search?q=%%22%s%%22" target="_blank">%s</a><br>
Height: %s m
"""

for lt, ln, el, name in zip(latitude, longitude, elevation, name):
    iframe = folium.IFrame(html=html % (name, name, el), width=200, height=100)
    features_volcanoes.add_child(folium.CircleMarker(location=[lt, ln], radius=6, popup=folium.Popup(iframe), fill_color=colorize(el), color='grey', fill_opacity=0.5))

map.add_child(features_volcanoes)

features_population = folium.FeatureGroup(name='Population')

features_population.add_child(folium.GeoJson(data=open('world.json', encoding='utf-8-sig').read(),
                                  style_function=lambda x: {'fillColor': 'green' if x['properties']['POP2005'] < 10000000
                                else 'orange' if 10000000 <= x['properties']['POP2005'] < 60000000 else 'red'}))


map.add_child(features_population)

map.add_child(folium.LayerControl())

map.save('Map1.html')


